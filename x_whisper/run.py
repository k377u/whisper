import argparse
import sys

from x_whisper.babelfish import load, transcribe, MODEL_NAMES

def main():

    parser = argparse.ArgumentParser(prog="babelfish", description="Complain")
    subparsers = parser.add_subparsers(help="help", dest="command")

    parser_transcribe = subparsers.add_parser("transcribe")
    parser_transcribe.add_argument("--model", choices=MODEL_NAMES, default="base")

    parser_load = subparsers.add_parser("load")
    parser_load.add_argument("--models", choices=MODEL_NAMES, nargs="+")

    args = parser.parse_args()
    if args.command == "transcribe":

        result = transcribe(args.model)
        sys.stdout.write('You said:\n"""\n')
        sys.stdout.write(result)
        sys.stdout.write('\n"""\n')
    elif args.command == "load":
        load(args.models)

    sys.exit(1)


if __name__ == "__main__":
    main()
