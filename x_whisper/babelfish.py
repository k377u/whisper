import sys
from typing import List
from pathlib import Path
import tempfile
import pyaudio
import wave
import whisper

MODEL_NAMES = whisper.available_models()


def load(models: List[str]):
    sys.stderr.write(f"Loading models: {models}\n")
    for model_name in models:
        sys.stderr.write(f"{model_name}\n")
        whisper.load_model(model_name)


def transcribe(model_size: str, translate=False):
    rate = 44100
    buffer_size = 1024
    channels = 1
    record_name = "record.wav"
    frames = []
    model = whisper.load_model(model_size)

    with tempfile.TemporaryDirectory() as tmpdir:
        directory = Path(tmpdir)
        file_path = str(directory.joinpath(record_name))

        audio = pyaudio.PyAudio()
        stream = audio.open(
            format=pyaudio.paInt16,
            channels=channels,
            rate=rate,
            input=True,
            frames_per_buffer=buffer_size,
        )
        sys.stderr.write("Recording started, do KeyboardInterrupt when ready\n")
        try:
            while True:
                data = stream.read(1024)
                frames.append(data)
        except KeyboardInterrupt:
            sys.stderr.write("\nRecording done\n")
        finally:
            stream.stop_stream()
            stream.close()
        audio.terminate()

        with wave.open(file_path, "wb") as sound_file:
            sound_file.setnchannels(channels)
            sound_file.setsampwidth(audio.get_sample_size(pyaudio.paInt16))
            sound_file.setframerate(rate)
            sound_file.writeframes(b"".join(frames))
            del frames

        sys.stderr.write("Processing audio\n")

        out = model.transcribe(
            file_path,
            fp16=False,
            verbose=False,
            task="translate" if translate else "transcribe",
        )
        return out["text"]
