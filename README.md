# Description

Testing openai whisper speech recognition. Transcribes spoken language to text. Can also do translate to english.


```
$ babel transcribe
  --model {tiny.en,tiny,base.en,base,small.en,small,medium.en,medium,large}
```
  
  
```
$ babel load
  --models {tiny.en,tiny,base.en,base,small.en,small,medium.en,medium,large} [{tiny.en,tiny,base.en,base,small.en,small,medium.en,medium,large} ...]
```


```
$ babel transcribe --model medium
Recording started, do KeyboardInterrupt when ready
^C
Recording done
Processing audio
Detected language: English
100%|██████████████████████████████████████████████████████████| 345/345 [00:16<00:00, 20.54frames/s]
You said:
"""
 It's a beautiful day.
"""
```
